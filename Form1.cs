﻿using Shapes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _1_oop_fundamentals_form
{
    public partial class MainForm : Form
    {
        private readonly List<Shape> shapes = new List<Shape>();
        private GraphicsDrawer gd;
        public MainForm()
        {
            InitializeComponent();

            Control panel = this.Controls.Find("DrawPanel", false)[0];
            this.gd = new GraphicsDrawer(panel.CreateGraphics());
            this.shapes.Add(new Square(50));
        }

        private void DrawButton_Click(object sender, EventArgs e)
        {
            
        }

        private void DrawPanel_Paint(object sender, PaintEventArgs e)
        {
            //Graphics g = new
            foreach (Shape shape in shapes)
            {
                shape.Draw(this.gd);
            }
        }
    }
}

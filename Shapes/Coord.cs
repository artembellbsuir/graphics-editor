﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1_oop_fundamentals_form.Shapes
{
    public class Coord
    {
        public float x, y;
        public Coord(float x, float y)
        {
            this.x = x;
            this.y = y;
        }
    }
}

﻿using _1_oop_fundamentals_form.Shapes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Shapes
{
    public abstract class Shape : IDrawable
    {
        public Coord center;
        public abstract float GetSquare();
        public abstract void Draw(GraphicsDrawer gd);
    }
}

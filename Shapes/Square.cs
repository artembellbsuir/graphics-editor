﻿using _1_oop_fundamentals_form.Shapes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Shapes
{
    class Square : Shape
    {
        public float width;

        public Square(float width)
        {
            this.center = new Coord(0, 0);
            this.width = width;
        }

        public override void Draw(GraphicsDrawer gd)
        {
            gd.DrawLineBetweenPoints(center, new Coord(center.x + width, center.y));
            gd.DrawLineBetweenPoints(new Coord(center.x + width, center.y), new Coord(center.x + width, center.y + width));
            gd.DrawLineBetweenPoints(new Coord(center.x + width, center.y + width), new Coord(center.x, center.y + width));
            gd.DrawLineBetweenPoints(new Coord(center.x, center.y + width), center);
        }

        public override float GetSquare()
        {
            return width * width;
        }
    }
}

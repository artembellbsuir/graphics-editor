﻿using _1_oop_fundamentals_form.Shapes;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace Shapes
{
    public class GraphicsDrawer
    {
        private Graphics graphics;
        private Pen pen = new Pen(Color.Black);
        private SolidBrush brush = new SolidBrush(Color.Red);
        public GraphicsDrawer(Graphics graphics)
        {
            this.graphics = graphics;


        }

        public void DrawLineBetweenPoints(Coord p1, Coord p2)
        {
            this.graphics.DrawLine(this.pen, p1.x, p1.y, p2.x, p2.y);
            //Console.WriteLine("Draw Line");
        }
    }
}

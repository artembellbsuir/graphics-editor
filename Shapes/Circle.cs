﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shapes
{
    class Circle : Shape
    {
        public float radius;
        public override void Draw(GraphicsDrawer gd)
        {
            gd.DrawLineBetweenPoints(center, center);
        }

        public override float GetSquare()
        {
            return (float)Math.PI * radius * radius;
        }
    }
}
